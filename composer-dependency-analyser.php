<?php

use ShipMonk\ComposerDependencyAnalyser\Config\Configuration;
use ShipMonk\ComposerDependencyAnalyser\Config\ErrorType;

$config = new Configuration;

return $config
    ->ignoreErrorsOnPackage('laravel/tinker', [ErrorType::UNUSED_DEPENDENCY]) // used for debugging via the command line
    ->ignoreErrorsOnPackage('lukasmu/laravel-favicon', [ErrorType::UNUSED_DEPENDENCY]) // ships with a service provider that is automatically loaded in the background
    ->ignoreErrorsOnPackage('lukasmu/laravel-glide', [ErrorType::UNUSED_DEPENDENCY]) // ships with a service provider that is automatically loaded in the background
    ->ignoreErrorsOnPackage('lukasmu/laravel-rickroll', [ErrorType::UNUSED_DEPENDENCY]); // ships with a service provider that is automatically loaded in the background
