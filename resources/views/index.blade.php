<x-layout description="This is the official website of Lukas Müller. Here you can explore his work and connect with him.">
    <section id="home" class="d-flex flex-column w-100 justify-content-center align-items-center">
        <x-glide::img src="mountains.jpg" alt="Lukas pauses on an exposed rock, taking in the vast valley below with quiet reverence on the Freiungen Höhenweg" sizes="(max-aspect-ratio: 3/2) 150vh, 100vw" class="object-fit-cover z-n1 position-absolute w-100" style="object-position: top left; height: 100vh;" />
        <div class="object-fit-cover position-top z-n1 position-absolute w-100" style="background: rgba(5, 13, 24, 0.3); height: 100vh;"></div>
        <div class="mt-auto hero-container w-50 z-1" style="padding-top:calc(3rem + 1.275rem + 0.3vw)">
            <p class="mb-0">
                <span class="text-bold">{{ ucfirst(__('hello')) }},</span> {{ __('nice to see you') }}!
                <br>
                {{ ucfirst(__('my name is')) }} <span class="text-bold">{{ config('app.name') }}</span> {{ __('and') }}
                <br>
                {{ __('I am') }} <span class="text-bold"><span id="typed"></span></span>
                <span id="typed-strings" class="d-none">
                    <span>full-stack {{ __('developer') }}</span>
                    <span>{{ __('graduated aerospace engineer') }}</span>
                    <span>{{ __('freelancer') }}</span>
                    <span>DevOps {{ __('specialist') }}</span>
                    <span>{{ __('in love with mountains') }}</span>
                    <span>{{ __('at home in Europe') }}</span>
                    <span>{{ __('open-source enthusiast') }}</span>
                </span>
            </p>
        </div>
        <ul id="iconbar" class="mt-auto list-group list-group-horizontal list-unstyled justify-content-center justify-content-md-end align-self-md-end p-4 text-white fs-4 flex-wrap z-1">
            <li>
                <a target="_blank" href="mailto:hello@lukasmu.com" aria-label="Mail">
                    <i class="fas fa-envelope"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="tel:+491629710675" aria-label="Phone">
                    <i class="fas fa-phone"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://api.whatsapp.com/send/?phone=491629710675" aria-label="Whatsapp" rel="noreferrer">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://linkedin.com/in/lukasmu" aria-label="LinkedIn" rel="noreferrer">
                    <i class="fab fa-linkedin"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://www.strava.com/athletes/lukasmu" aria-label="Strava" rel="noreferrer">
                    <i class="fab fa-strava"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://www.youtube.com/channel/UCyPWNRE3hLS2n3NsTAEWUhQ" aria-label="YouTube" rel="noreferrer">
                    <i class="fab fa-youtube"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://pypi.org/user/LukasMueller" aria-label="PyPi" rel="noreferrer">
                    <i class="fa-brands fa-python"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://packagist.org/users/lukasmu/" aria-label="Packagist" rel="noreferrer">
                    <i class="fab fa-php"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="{{ asset('lukasmu.pub') }}" aria-label="Public SSH key">
                    <i class="fas fa-key"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://github.com/lukasmu" aria-label="GitHub" rel="noreferrer">
                    <i class="fab fa-github"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://gitlab.com/lukasmu" aria-label="GitLab" rel="noreferrer">
                    <i class="fab fa-gitlab"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://nextcloud.lukasmu.com" aria-label="Nextcloud" rel="noreferrer">
                    <i class="fas fa-cloud"></i>
                </a>
            </li>
            <li>
                <a target="_blank" href="https://invoices.lukasmu.com" aria-label="Invoices" rel="noreferrer">
                    <i class="fas fa-file-invoice-dollar"></i>
                </a>
            </li>
        </ul>
    </section>
    {{-- TODO: Add a section per menu item here! --}}
    @push('scripts')
        <script>
            console.log("%c\n          /\\\n         /**\\\n        /****\\   /\\\n       /      \\ /**\\\n      /  /\\    /    \\        /\\    /\\  /\\      /\\            /\\/\\/\\  /\\\n     /  /  \\  /      \\      /  \\/\\/  \\/  \\  /\\/  \\/\\  /\\  /\\/ / /  \\/  \\\n    /  /    \\/ /\\     \\    /    \\ \\  /    \\/ /   /  \\/  \\/  \\  /    \\   \\\n   /  /      \\/  \\/\\   \\  /      \\    /   /    \\\n__/__/_______/___/__\\___\\__________________________________________________\n           _          _               __  __ _   _ _ _\n          | |   _   _| | ____ _ ___  |  \\/  (_) (_) | | ___ _ __\n          | |  | | | | |/ / _` / __| | |\\/| | | | | | |/ _ \\ '__|\n          | |__| |_| |   < (_| \\__ \\ | |  | | |_| | | |  __/ |\n          |_____\\__,_|_|\\_\\__,_|___/ |_|  |_|\\__,_|_|_|\\___|_|\n\nDo you like this page?\nAre you interested in the source code of this page?\nIt's on https://gitlab.com/lukasmu/portfolio!\nHave fun & climb mountains!", "font-family: monospace;");
        </script>
    @endpush
</x-layout>