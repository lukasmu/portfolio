<div class="sidebar-header p-4">
    <a href="{{ LaravelLocalization::localizeUrl('/') }}">
        <h1 class="fs-4 text-white text-center fw-bold">{{ config('app.name') }}</h1>
    </a>
</div>
<ul class="list-unstyled p-4 mb-auto">
    <li>
        <a href="{{ LaravelLocalization::localizeUrl('/') }}" class="active"><i class="fas fa-home me-2"></i> {{ ucfirst(__('home')) }}</a>
    </li>
    <li>
        <a href="{{ LaravelLocalization::localizeUrl('/#about') }}"><i class="fas fa-address-card me-2"></i> {{ ucfirst(__('about')) }}</a>
    </li>
    <li>
        <a href="{{ LaravelLocalization::localizeUrl('/#portfolio') }}"><i class="fas fa-robot me-2"></i> {{ ucfirst(__('portfolio')) }}</a>
    </li>
    <li>
        <a href="{{ LaravelLocalization::localizeUrl('/#qa') }}"><i class="fas fa-question-circle me-2"></i> Q &amp; A</a>
    </li>
    <li>
        <a href="{{ LaravelLocalization::localizeUrl('/#contact') }}"><i class="fas fa-mail-bulk me-2"></i> {{ ucfirst(__('contact')) }}</a>
    </li>
    <li class="alert alert-warning alert-soon">
        <i class="fas fa-tools"></i> {{ ucfirst(__('coming soon')) }}!
    </li>
</ul>
<div class="text-center p-4">
    <a href="{{ LaravelLocalization::localizeUrl('/privacy') }}">{{ ucfirst(__('privacy')) }}</a> | <a href="{{ LaravelLocalization::localizeUrl('/imprint') }}">{{ ucfirst(__('imprint')) }}</a>
    <br>
    &copy; 1994-{{ date("Y") }} {{ config('app.name') }}
</div>
