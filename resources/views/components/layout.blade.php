<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#293042">
        @if($description)<meta name="description" content="{{ $description }}">@endif

        <title>{{ config('app.name') }}@if($title) - {{ $title }}@endif</title>

        @include('favicon::components.links')
        @vite('resources/css/app.scss')
    </head>
    <body class="d-flex align-items-stretch w-100 bg-dark">
        <nav id="sidebar" class="d-flex flex-column bg-dark active">
            <x-nav />
        </nav>

        <main id="content" class="active z-1">
            <div id="topbar" class="align-self-start w-100 text-white fs-4 position-fixed">
                <button class=" fs-4 p-4 hamburger hamburger--vortex" type="button" aria-label="{{ ucfirst(__('menu')) }}" aria-controls="sidebar" data-immerser-solid="menu">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
                </button>
                <span class="position-fixed ml-auto end-0">
                    <select class="form-select form-select-language bg-transparent text-white border-0 p-4 fs-4 mb-0" aria-label="{{ ucfirst(__('language')) }}" onchange="javascript:location.href=this.value;">
                        {{-- TODO: Fix option text color in Chrome --}}
                        <option value="{{ LaravelLocalization::getLocalizedURL('en') }}" class="p-0" @if(app()->getLocale() == 'en') selected @endif>EN</option>
                        <option value="{{ LaravelLocalization::getLocalizedURL('de') }}" class="p-0" @if(app()->getLocale() == 'de') selected @endif>DE</option>
                        <option value="{{ LaravelLocalization::getLocalizedURL('nl') }}" class="p-0" @if(app()->getLocale() == 'nl') selected @endif>NL</option>
                    </select>
                </span>
            </div>

            {{ $slot }}
        </main>

        @vite('resources/js/app.js')
        @stack('scripts')
    </body>
</html>
