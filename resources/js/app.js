import './bootstrap';

// Bootstrap
import * as Popper from '@popperjs/core';
window.Popper = Popper;
import 'bootstrap';

// Typed
import Typed from 'typed.js';
if (document.getElementById("typed")) {
    new Typed('#typed', {
        stringsElement: '#typed-strings',
        loop: true,
        typeSpeed: 100,
        backSpeed: 50,
        backDelay: 2000
    });
}

// ScrollMagic
import ScrollMagic from 'scrollmagic';
let scrollMagicInit = function () {
    let scene;
    let controller = new ScrollMagic.Controller();
    let items = document.querySelectorAll("section");

    items.forEach(function(element) {
        let height = element.clientHeight;
        scene = new ScrollMagic.Scene({
            duration: height,
            triggerElement: element,
            triggerHook: 0,
            reverse: true
        }).on("enter leave", function() {
            if (element.classList.contains("topbar-dark")) {
                document.querySelector('#topbar').classList.add("dark");
            } else {
                document.querySelector('#topbar').classList.remove("dark");
            }
        }).on("enter", function() {
            if (element.id && element.id!='home') {
                history.pushState(null, null, '#'+element.id);
            } else {
                history.pushState('', document.title, window.location.pathname + window.location.search);
            }
        }).addTo(controller);
    });
}
scrollMagicInit();
window.addEventListener("resize", scrollMagicInit);


// Sidebar toggling
document.querySelector('#topbar button').addEventListener('click', (e) => {
    document.querySelector('#sidebar').classList.toggle('active');
    document.querySelector('#content').classList.toggle('active');
    document.querySelector('#topbar button').classList.toggle('is-active');
});

// Highlight current menu items (in the sidebar)
// TODO: Repair the script below
// let url_current = location.protocol + '//' + location.host + location.pathname;
// let navbar_links = document.querySelectorAll('nav ul li.nav-item a');
// for (let i = 0; i < navbar_links.length; i++) {
//    let clean_url = navbar_links[i].getAttribute('href').split('?')[0]
//    if(url_current.startsWith(clean_url)){
//        navbar_links[i].closest('li.nav-item').classList.add('active');
//    }
// }
