ARG VARIANT=franken

###########
# Franken #
###########

FROM dunglas/frankenphp:1.4.4-php8.3.17-alpine AS franken

# Overwrite the entrypoint script
COPY docker-php-entrypoint /usr/local/bin/docker-php-entrypoint
RUN chmod +x /usr/local/bin/docker-php-entrypoint

###########################
# PHP Extension Installer #
###########################

FROM mlocati/php-extension-installer:2.7.27 AS php-extension-installer

#######
# PHP #
#######

FROM php:8.3.17-fpm-alpine3.21 AS php

# Install the PHP extension installer
COPY --from=php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

############
# Composer #
############

FROM composer:2.8.5 AS composer

########
# Node #
########

FROM node:23.9.0-alpine3.21 AS node

WORKDIR /app

COPY package.json package-lock.json vite.config.js /app/
COPY resources /app/resources

RUN npm install && npm run build

########
# Main #
########

# hadolint ignore=DL3006
FROM ${VARIANT} AS app

# Set the working directory
WORKDIR /app

# Create the PsySH config directory
RUN mkdir -p /config/psysh

# Add labels
# For list of labels see https://github.com/opencontainers/image-spec/blob/main/annotations.md
# Some labels such as org.opencontainers.image.version should probably only be declared at end of the Dockerfile
LABEL org.opencontainers.image.title="Lukas Müller"
LABEL org.opencontainers.image.source="https://gitlab.com/lukasmu/portfolio"
LABEL org.opencontainers.image.authors="hello@lukasmu.com"
LABEL org.opencontainers.image.licenses="MIT"

# Use the default production configuration for PHP
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# Increase the memory limit
RUN sed -i 's/memory_limit = .*/memory_limit = 512M/' "$PHP_INI_DIR/php.ini"

# Install composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Install additional PHP extensions
RUN install-php-extensions \
    exif \
    gd \
    intl \
    pcntl

# Switch to non-root user
ARG USER=www-data
RUN \
    # Add user
    useradd ${USER}; \
    # Remove default capability
    setcap -r /usr/local/bin/frankenphp; \
    # Give write access to the proxy directories
    chown -R ${USER}:${USER} /data/caddy && chown -R ${USER}:${USER} /config/caddy; \
    # Give write access to the application directory
    chown ${USER}:${USER} /app; \
    # Give write access to the  PsySH config directory
    chown ${USER}:${USER} /config/psysh
USER ${USER}

# Install only the PHP packages required for production
COPY --chown=www-data:www-data composer.json composer.lock /app/
RUN composer install \
    --no-cache \
    --no-autoloader \
    --no-interaction \
    --no-progress \
    --no-dev \
    --no-scripts \
    --no-ansi

# Copy assets
COPY --chown=${USER}:${USER} --from=node /app/public/build /app/public/build

# Copy main application files
# TODO: Once --exclude works, make sure to exclude some files, e.g. Dockerfile, .dockerignore, package.json, package-lock.json, vite.config.js, etc.
COPY --chown=${USER}:${USER} . /app

# Optimize
RUN composer dump-autoload \
  --classmap-authoritative \
  --no-dev \
  --no-ansi

# Optimize
RUN php artisan storage:link && \
    php artisan event:cache && \
    php artisan route:trans:cache && \
    php artisan view:cache && \
    php artisan favicon:cache

# Add version information
ARG APP_VERSION='n/a'
LABEL org.opencontainers.image.version=$APP_VERSION
ENV APP_VERSION=$APP_VERSION

# Note that the image that can be generated from this Dockerfile contains files that are not necessarily needed for
# running the application in a production environment (e.g. the php-extension-installer and composer binaries,
# the tests directory, the composer.json and package.json files). However, these files are relatively small and thus do
# not really impact the image size. While one could get rid of these files by adding more build stages, this would make
# the entire CI/CD process more complex.