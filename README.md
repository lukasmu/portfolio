# Portfolio of Lukas Müller

[![coverage report](https://gitlab.com/lukasmu/portfolio/badges/main/coverage.svg?style=flat-square)](https://gitlab.com/lukasmu/portfolio/-/commits/main)
[![pipeline status](https://gitlab.com/lukasmu/portfolio/badges/main/pipeline.svg?style=flat-square)](https://gitlab.com/lukasmu/portfolio/-/commits/main)

This repository contains the code for the personal website of Lukas Müller available at [lukasmu.com](https://lukasmu.com).

## History

Lukas Müller has been developing many websites/apps/programs/workflows next to his studies/work since more than 10 years. But until now he never had any time left to develop his very own website.

Technically, he still does not have time for such things... But he recently made an effort to start coding his own portfolio. Here it is!

## Installing

In the following some instructions for installing the portfolio in a local development environment are presented. The entire installation process should not take much longer than 10 minutes.

It is assumed that you are using Windows 10 or Windows 11 as operating system. In case that you are using Linux or Mac you need to do things slightly different, but usually the process is even more straightforward.

1. Make sure to install a GIT client (e.g. [the native one](https://git-scm.com/downloads)) and an IDE of your choice (e.g. [PHPStorm](https://www.jetbrains.com/phpstorm/)).
2. Then install Visual C++ Redistributable for Visual Studio, PHP and Composer as explained on [https://devanswers.co/install-composer-php-windows-10/](https://devanswers.co/install-composer-php-windows-10/). Make sure to install at least PHP 8.2.
3. Go to the directory where you installed PHP, search for the php.ini file and enable the extension fileinfo by removing the semicolon before the line extension=fileinfo. Save the php.ini file.
4. Then clone this repository to your computer. Simply run ```git clone https://gitlab.com/lukasmu/portfolio.git``` in the directory of your choice. Afterwards switch to the cloned directory by typing ```cd portfolio```.
5. Run  ```composer create-project``` to finalize the installation of the portfolio. This might take a minute or two.
6. Run ```php artisan serve``` to startup your local development server. If you want to stop the server press CTRL and C.

## Testing

You can run all tests by calling ```php artisan test```. Make sure that your local development server is running in the background for the browser tests!

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.
Hereafter some development tips & tricks can be shared.

- Very important tip: Add lots of easter eggs!
- To be continued...

## TODOs

Some things that still need to be done:

- Add sections similar to the ones on https://gilbertndresaj.com
- Add a Q&A section similar to the one on https://www.billchien.net/about
- Add big call-to-action buttons leading to a contact form
- Change the main font to something less common
- Include download for the AVV on the privacy page
- Include download for list of subprocessors on the privacy page
- Align the page with LinkedIn even more 
- Use Laravel Octane as soon as issues with mcamara/laravel-localizatio have been resolved: https://github.com/mcamara/laravel-localization/issues/927#issuecomment-2652126596
- Check the source for further TODOs

## Help

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Security

If you discover a security vulnerability, please send an e-mail to Lukas Müller via [hello@lukasmu.com](mailto:hello@lukasmu.com). All security vulnerabilities will be promptly addressed.

## License

This portfolio is open-sourced software licensed under the MIT license. Please see [LICENSE](LICENSE.md) for details.
