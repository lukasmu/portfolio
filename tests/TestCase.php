<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Mcamara\LaravelLocalization\LaravelLocalization;
use DOMDocument;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function refreshApplicationWithLocale($locale): void
    {
        self::tearDown();
        putenv(LaravelLocalization::ENV_ROUTE_KEY . '=' . $locale);
        self::setUp();
    }

    protected function tearDown(): void
    {
        putenv(LaravelLocalization::ENV_ROUTE_KEY);
        parent::tearDown();
    }

    /**
     * @param $html
     */
    public function assertValidHtml($html)
    {
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML($html);
        $errors = collect(libxml_get_errors());
        $errors = $errors->filter(function ($error) {
            return !in_array($error->message, [
                "Tag nav invalid\n",
                "Tag header invalid\n",
                "Tag main invalid\n",
                "Tag footer invalid\n",
                "Tag section invalid\n",
            ]);
        });
        if ($errors->count() > 0) {
            $lines = preg_split("/((\r?\n)|(\r\n?))/", $html);
            $lines = $errors->map(function ($error) use ($lines) {
                return implode("\n", [
                    trim($lines[$error->line-2]),
                    trim($lines[$error->line-1]),
                    trim($lines[$error->line]),
                    sprintf('Error: %s', $error->message)
                ]);
            });
            $this->fail(sprintf("The HTML is probably invalid near:\n%s", $lines->implode("\n\nAnd near:\n")));
        }
        $this->assertTrue(true);
    }
}
