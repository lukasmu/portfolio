<?php

namespace Tests\Feature;

use Tests\TestCase;

class BasicTest extends TestCase
{
    public function test_static_pages_availability(): void
    {
        $this->get('/')->assertStatus(302);
        $this->get('/privacy')->assertStatus(302);
        $this->get('/imprint')->assertStatus(302);

        $this->refreshApplicationWithLocale('en');
        $this->get('/en')->assertStatus(200);
        $this->get('/en/privacy')->assertStatus(200);
        $this->get('/en/imprint')->assertStatus(200);

        $this->refreshApplicationWithLocale('de');
        $this->get('/de')->assertStatus(200);
        $this->get('/de/privacy')->assertStatus(200);
        $this->get('/de/imprint')->assertStatus(200);

        $this->refreshApplicationWithLocale('nl');
        $this->get('/nl')->assertStatus(200);
        $this->get('/nl/privacy')->assertStatus(200);
        $this->get('/nl/imprint')->assertStatus(200);
    }

    public function test_static_pages_validity(): void
    {
        $this->refreshApplicationWithLocale('en');
        $this->assertValidHtml($this->get('/en')->getContent());
        $this->assertValidHtml($this->get('/en/privacy')->getContent());
        $this->assertValidHtml($this->get('/en/imprint')->getContent());

        $this->refreshApplicationWithLocale('de');
        $this->assertValidHtml($this->get('/de/privacy')->getContent());
        $this->assertValidHtml($this->get('/de/imprint')->getContent());
    }
}
