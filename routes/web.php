<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter;
use Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect;

Route::prefix(LaravelLocalization::setLocale())
    ->middleware([LaravelLocalizationRedirectFilter::class, LocaleSessionRedirect::class])
    ->group(function () {
        Route::view('', 'index');
        Route::view('imprint', 'imprint');
        Route::view('privacy', 'privacy');
    });
