# Changelog

All notable changes to this portfolio will be documented in this file.

## v1.0.0 - 2020-02-02

- Initial release

## v1.0.1 - 2022-11-05

- Framework upgrade (→ Laravel 9)

## v1.0.2 - 2023-04-20

- Framework upgrade (→ Laravel 10)

## v2.0.0 - 2025-03-04

- Framework upgrade (→ Laravel 12)
- Modernization
- Dockerization